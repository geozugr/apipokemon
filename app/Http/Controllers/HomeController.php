<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    /**Retorna la vista home que contiene el listado de los pokemones */
    {
        return view('home');
    }

    public function viewPokemon(Request $request)
    /**Retorna la vista que contiene el prop url para ver los detalles del pokemon*/
    {
        $url = $request->query('url');
        return view('pokemon')->with('url', $url);
    }
}
