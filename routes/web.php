<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Auth::routes();

/**Obtiene la vista home que contiene el listado de los pokemones */
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home'); 

/**Obtiene la vista pokemon que contiene el detalle de los pokemones
 * Se requiere estar autenticado para acceder a esta vista
 */
Route::get('/pokemon', [App\Http\Controllers\HomeController::class, 'viewPokemon'])->name('pokemon')->middleware('auth');
