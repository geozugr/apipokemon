@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Pokemon</div>
                <a class="btn btn-primary col-4" href="/home">Ver todos los pokemones</a>
                <div class="card-body">
                    <pokemon-detail url="{{$url}}"></pokemon-detail>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection