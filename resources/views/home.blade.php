@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-h">
                <div class="card-header">{{ __('Listado de Pokemones') }}</div>
                <div class="card-body">
                   <pokemon-list> </pokemon-list>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
